OvalSplints are designed and manufactured in Britain, featuring 3 points of pressure that protect and support joints that are unstable & not yet healed. Several different ways can be worn OvalSplints to treat a variety of common ailments like Trigger finger, Trigger thumb, Swan Neck, Boutonniere deformities, hypermobility, and fracture protection.

Website: https://www.ovalsplints.com/
